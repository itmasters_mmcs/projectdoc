start concat_files.exe
del docs.html
del docs_title.html
del docs.pdf
pandoc -S --toc --toc-depth=1 -s -c mycss.css docs.md -o docs.html
pandoc -c title_mycss.css docs_title.md -o docs_title.html
wkhtmltopdf docs_title.html docs.html docs.pdf